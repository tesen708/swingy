package swingy.model.Artifacts;

public enum ArtifactTypes {
    RING,
    SWORD,
    SHIELD,
    SPIKE,
    ROD,
    HELMET,
    CHESTPLATE,
    LEGGINS,
    BOOTS
}
