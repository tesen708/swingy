package swingy.model.Artifacts;

import swingy.model.Colors;
import swingy.model.Heroes.Hero;
import swingy.model.Heroes.HeroTypes;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public abstract class Artifact {

    protected String name;
    protected ArtifactTypes artifactType;
    protected String description;
    protected Requirement requirement;
    protected Effects effects;
    
    private final NumberFormat formatter = new DecimalFormat("#0.00");

    public Artifact() {
        this.requirement = new Requirement();
        this.effects = new Effects();
        generateRequirementAndEffects();
    }

    public abstract void generateRequirementAndEffects();

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getRequirementAndEffects(Hero withHero) {
        StringBuilder description = new StringBuilder();

        if (this.requirement != null) {
            description.append("Требования: \n");

            // Описание - нужные классы
            if (this.requirement.requiredClasses != null && this.requirement.requiredClasses.length != 0) {
                description.append("Подходит для классов: ");
                for (int i = 0; i < this.requirement.requiredClasses.length; i++) {
                    if (this.requirement.requiredClasses[i].equals(withHero.getHeroClass())) {
                        description.append(Colors.GREEN);
                    } else {
                        description.append(Colors.RED);
                    }
                    description.append(this.requirement.requiredClasses[i])
                            .append(Colors.RESET);
                    if (i + 1 < this.requirement.requiredClasses.length) {
                        description.append(", ");
                    }
                }
                description.append("\n");
            }

            // Описание - заблокированные классы
            if (this.requirement.bannedClasses != null && this.requirement.bannedClasses.length != 0) {
                description.append("Не подходит для классов: ");
                for (int i = 0; i < this.requirement.bannedClasses.length; i++) {
                    if (!this.requirement.bannedClasses[i].equals(withHero.getHeroClass())) {
                        description.append(Colors.RED);
                    } else {
                        description.append(Colors.GREEN);
                    }
                    description.append(this.requirement.bannedClasses[i])
                            .append(Colors.RESET);
                    if (i + 1 < this.requirement.bannedClasses.length) {
                        description.append(", ");
                    }
                }
                description.append("\n");
            }

            // Описание - необходимый уровень
            if (this.requirement.requiredLevel > 0) {
                description.append("Необходим уровень: ");
                if (this.requirement.requiredLevel <= withHero.getLevel()) {
                    description.append(Colors.GREEN);
                } else {
                    description.append(Colors.RED);
                }
                description.append(this.requirement.requiredLevel)
                        .append(Colors.RESET)
                        .append("\n");
            }
            description.append("\n");
        }

        if (this.effects == null) {
            description.append("Косметический предмет.\n");
        } else {
            // Описание - эффекты
            // Описание - бонус к хп
            if (this.effects.hpBoost != 0) {
                description.append(describeEffect("Бонус к Здоровью: ", this.effects.hpBoost));
            }
            // Описание - бонус к дамагу
            if (this.effects.attackBoost != 0) {
                description.append(describeEffect("Бонус к Атаке: ", this.effects.attackBoost));
            }
            // Описание - бонус к защите
            if (this.effects.defenceBoost != 0) {
                description.append(describeEffect("Бонус к Защите: ", this.effects.defenceBoost));
            }
            // Описание - бонус к критическому урону
            if (this.effects.criticalBoost != 0) {
                description.append(describeEffect("Шанс нанести критический урон: ", this.effects.criticalBoost));
            }
            // Описание - бонус к опыту
            if (this.effects.expBoost != 0) {
                description.append(describeEffect("Прирост опыта: ", this.effects.expBoost));
            }
            // Описание - бонус к луту
            if (this.effects.lootBoost != 0) {
                description.append(describeEffect("Шанс выпадения предмета: ", this.effects.lootBoost));
            }
            // Описание - бонус к побегу
            if (this.effects.escapeChanceBoost != 0) {
                description.append(describeEffect("Вероятность успешного побега: ", this.effects.escapeChanceBoost));
            }
            // Описание - бонус к уклонению
            if (this.effects.evadeChanceBoost != 0) {
                description.append(describeEffect("Вероятность уклонения: ", this.effects.evadeChanceBoost));
            }

            // Описание - прочие эффекты
            if (this.effects.poisonResist) {
                description.append(Colors.GREEN)
                        .append("Защита от отравления")
                        .append(Colors.RESET)
                        .append("\n");
            }
        }

        return description.toString();
    }

    private String describeEffect(String effectDesc, double bonus) {
        StringBuilder str = new StringBuilder(effectDesc);
        str.append(": ");
        if (bonus > 0) {
            str.append(Colors.GREEN).append("+");
        } else {
            str.append(Colors.RED);
        }
        str.append(formatter.format(bonus))
                .append("%")
                .append(Colors.RESET)
                .append("\n");

        return str.toString();
    }

    // Надо бы разнести это в отдельные классы. С другой стороны, ничего кроме этого класса их не использует...
    protected class Requirement{
        protected int requiredLevel;
        protected HeroTypes[] requiredClasses;
        protected HeroTypes[] bannedClasses;

        // Не уверен, что такие требования нужны
//        private double requiredMaxHp;
//        private double requiredMinHp;
//        private double requiredMaxAttack;
//        private double requiredMinAttack;
//        private double requiredMaxDefence;
//        private double requiredMinDefence;

    }

    protected class Effects {
        protected double expBoost;
        protected double hpBoost;
        protected double attackBoost;
        protected double defenceBoost;
        protected double criticalBoost;
        protected double lootBoost;
        protected double escapeChanceBoost;
        protected double evadeChanceBoost;
        protected boolean poisonResist;

    }
}
