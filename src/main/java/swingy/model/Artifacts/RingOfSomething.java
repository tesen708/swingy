package swingy.model.Artifacts;

import swingy.model.Heroes.HeroTypes;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static swingy.model.Artifacts.ArtifactTypes.*;

public class RingOfSomething extends Artifact {

    public RingOfSomething() {
        this.name = "Какое-то кольцо";
        this.description = "Странное кольцо, которое я обнаружил у себя.";
        this.artifactType = RING;

    }

    @Override
    public void generateRequirementAndEffects() {
        this.requirement.requiredClasses = new HeroTypes[]{HeroTypes.FIRST_TYPE, HeroTypes.SECOND_TYPE};
        this.requirement.bannedClasses = null;
        this.requirement.requiredLevel = 1;

        this.effects = null;
//        this.effects.hpBoost = 0.50;
//        this.effects.expBoost = 1.00;
//        this.effects.escapeChanceBoost = 5.00;
//        this.effects.criticalBoost = -1.00;
    }

}
