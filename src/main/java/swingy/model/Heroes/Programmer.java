package swingy.model.Heroes;

import javax.validation.constraints.NotEmpty;

public class Programmer extends Hero {
    public Programmer(@NotEmpty String name) {
        super(name, HeroTypes.FIRST_TYPE);
    }

    @Override
    protected void generateStats() {
        this.maxHp = 1000.00;
        this.attack = 10.00;
        this.defence = 10.00;
    }
}
