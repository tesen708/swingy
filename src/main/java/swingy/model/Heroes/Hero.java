package swingy.model.Heroes;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;

public abstract class Hero {

    private final String name;
    private final HeroTypes heroType;
    @PositiveOrZero
    private double hp;
    @PositiveOrZero
    protected double maxHp;
    @PositiveOrZero
    protected double attack;
    @PositiveOrZero
    protected double defence;
    @PositiveOrZero
    private int level;
    @PositiveOrZero
    private double exp;
    @PositiveOrZero
    private double expForNextLevel;

    public Hero(@NotEmpty String name, @NotEmpty HeroTypes type) {
        this.name = name;
        this.heroType = type;
        this.level = 0;
        this.exp = 0;

        System.out.println("name = " + name + ", type = " + type);

        generateStats();
        updateExpForNextLevel();
        this.hp = this.maxHp;
    }

    protected abstract void generateStats();

    public void takeDamage(double damage) {
        this.hp -= damage;
        if (this.hp <= 0.00) {
            this.hp = 0.00;
            die();
        } else if (this.hp > this.maxHp) { // Для подхила (можно написать еще метод, или переименовать этот)
            this.hp = this.maxHp;
        }
    }

    private void die() {
        // LOL, YOU DEAD!
    }

    public void takeExp(double exp) {
        this.exp += exp;
        if (this.exp >= this.expForNextLevel) {
            levelUp();
        }
    }

    private void levelUp() {
        level += 1;
        exp -= expForNextLevel;
        updateExpForNextLevel();
    }

    private void updateExpForNextLevel() {
        this.expForNextLevel = level * 1000 + (Math.pow((level - 1), 2)) * 450;
    }

    public String getName() {
        return name;
    }

    public HeroTypes getHeroClass() {
        return heroType;
    }

    public double getHp() {
        return hp;
    }

    public double getAttack() {
        return attack;
    }

    public double getDefence() {
        return defence;
    }

    public int getLevel() {
        return level;
    }

    public double getExp() {
        return exp;
    }

    public double getMaxHp() {
        return maxHp;
    }

}
