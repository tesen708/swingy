package swingy;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import java.util.Arrays;

public class Application implements Runnable {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Application());
        System.out.println("pepega = " + Arrays.deepToString(args));
    }

    public void run() {

        // Создаем окно с заголовком "Hello, World!"

        JFrame f = new JFrame ("Hello, World!");

        // Ранее практиковалось следующее: создавался listener и регистрировался
        // на экземпляре главного окна, который реагировал на windowClosing()
        // принудительной остановкой виртуальной машины вызовом System.exit().
        // Теперь же есть более "правильный" способ задать реакцию на закрытие окна.
        // Данный способ уничтожает текущее окно, но не останавливает приложение. Тем
        // самым приложение будет работать, пока не будут закрыты все окна.

        f.setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE);

        // однако можно задать и так:
        //            f.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

        // Добавляем на панель окна нередактируемый компонент с текстом.

        //f.getContentPane().add (new JLabel("Hello, World!")); - старый стиль
        f.add(new JLabel("Hello World"));
        f.setResizable(false);

        // pack() "упаковывает" окно до оптимального размера, рассчитанного на основании размеров
        // всех расположенных в нём компонентов.

        f.pack();

        // Показать окно

        f.setVisible(true);
    }
}