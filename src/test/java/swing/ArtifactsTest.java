package swing;

import org.junit.jupiter.api.Test;
import swingy.model.Artifacts.Artifact;
import swingy.model.Artifacts.RingOfSomething;
import swingy.model.Heroes.Hero;
import swingy.model.Heroes.HeroTypes;
import swingy.model.Heroes.Programmer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ArtifactsTest {

    @Test
    void createArtifact() {
        Hero myHero = new Programmer("mister X");

        Artifact ring = new RingOfSomething();
        System.out.println(ring.getName() + ring.getDescription() + ring.getRequirementAndEffects(myHero));
    }

}
